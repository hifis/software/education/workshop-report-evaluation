# Workshop Report Evaluation

#### Background

This Project was started to conclude the workshop reports automatical to one overview and give some statistics about the Utilization or the coverage to draw conclusions whether more workshops should be organized or to see how many people not show up.

#### Instalation

Download the skript and run it.

#### Maintainers

- Fredo Erxleben

#### Contributing

Feel free to dive in! Open an issue or submit PRs.

#### Contributors

This project exists thanks to all the people who contribute. 

- Lida Berthold
- Gero Beckmann
- Fredo Erxleben




