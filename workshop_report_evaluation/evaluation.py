from datetime import datetime

import prettytable
from prettytable import PrettyTable
from typing import List

from report import Report


def list_sources(reports: List[Report]) -> str:
    """
    Generate a list of all reports that were included in the evaluation.
    Args:
        reports:
            A list of reports which are to be listed as sources.

    Returns:
        A markdown-formatted text providing the list of all included reports.
    """
    reports.sort(key=lambda x: x.file_name)
    sources = "\n".join(
        [f"* {report.file_name}" for report in reports]
    )
    return f"### Sources\n{sources}\n"


def generate_summary(reports: List[Report]) -> str:
    """
    Generate an overall summary based on the provided reports.
    Args:
        reports:
            A list of reports which are to be included in the summary.

    Returns:
        A markdown-formatted text providing the summary of the reports.
    """
    overall_capacity = sum(report.capacity for report in reports)
    overall_demand = sum(report.total_demand for report in reports)
    overall_selected = sum(report.total_selected for report in reports)
    overall_attendees = sum(report.total_attendees for report in reports)
    overall_participant_hours = sum(report.participant_hours for report in reports)
    overall_duration = sum(report.duration for report in reports)
    # NOTE This feels clunky and unwieldy. There must be a better way to
    # express this…

    return (
        f"### Summary\n"
        f"* Total reports: {len(reports)}\n"
        f"* Overall Capacity: {overall_capacity}\n"
        f"* Overall Demand: {overall_demand}\n"
        f"* Overall Selected: {overall_selected}\n"
        f"* Overall Attendees: {overall_attendees}\n"
        f"* Overall Duration: {overall_duration}\n"
        f"* Overall participant hours: {overall_participant_hours}\n"
    )


def evaluate_nps(reports: List[Report]) -> str:
    """
    Do an evaluation of all provided net promoter scores from the reports.
    Args:
        reports:
            A list of reports which may have NPS given. Reports without a
            reported NPS are ignored for this evaluation.

    Returns:
        A markdown-formatted text giving the result of the NPS evaluation.
    """
    nps_scores = []
    nps_weighted = 0
    total_n = 0

    for report in reports:
        if report.nps:
            if report.nps.score < 80:
                print(
                    f"NPS {report.nps.score} n={report.nps.n} "
                    f"in {report.file_name}"
                )
            nps_scores.append(report.nps.score)
            nps_weighted += (report.nps.score * report.nps.n)
            total_n += report.nps.n
    nps_weighted /= total_n
    nps_scores.sort()  # From lowest, ascending

    return (
        f"### Net Promoter Score\n"
        f"* 3 Lowest NPS: {nps_scores[:3]}\n"
        f"* 3 Highest NPS: {nps_scores[-1:-4:-1]}\n"
        f"* Avg. NPS: {sum(nps_scores) / len(nps_scores)}\n"
        f"* Weighted avg. NPS: {nps_weighted}\n"
        f"  * n = {total_n}\n"
    )


def list_participation_details(reports: List[Report]) -> str:
    """
    Print a table with participation details on all selected reports.

    This includes demand, selected participants, attendance and derived values.

    Args:
        reports:
            The list of reports for which the details are to be printed.

    Returns:
        A markdown-formatted table including the details of all reports.
    """
    reports.sort(key=lambda r: r.dates[0])

    metrics = {
        "Date": lambda r: r.dates[0],
        "Capacity": lambda r: r.capacity,
        "Demand": lambda r: r.total_demand,
        "Selected": lambda r: r.total_selected,
        "Attendees": lambda r: r.total_attendees,
        "Coverage %": lambda r: r.coverage,
        "Booking %": lambda r: r.booking,
        "No-Show %": lambda r: r.no_show_rate,
        "Utilization %": lambda r: r.utilization
    }

    detail_table = PrettyTable(
        field_names=metrics.keys(), align="r"
    )
    detail_table.set_style(prettytable.MARKDOWN)

    for report in reports:
        row_data = [
            f"{evaluation(report):4.0%}" if "%" in metric
            else f"{evaluation(report)}"
            for metric, evaluation in metrics.items()
        ]
        detail_table.add_row(row_data)

    return f"### Participation Statistics\n{detail_table}\n"


def create_outreach_table(reports: List[Report]) -> str:
    """
    Create an overview table of the outreach of workshop providers.

    Args:
        reports:
            The list of reports from which the outreaches are to be taken.

    Returns:
        A markdown-formatted table summarizing the outreach towards each of
        the centers.
    """
    centers = set()
    demand_all = dict()
    selected_all = dict()
    attendees_all = dict()

    for report in reports:
        for outreach in report.outreach:
            to_center = outreach.to_center

            demand_all[to_center] = \
                demand_all.get(to_center, 0) + outreach.demand
            selected_all[to_center] = \
                selected_all.get(to_center, 0) + outreach.selected
            attendees_all[to_center] = \
                attendees_all.get(to_center, 0) + outreach.attendees
            centers.add(to_center)

    outreach_table = PrettyTable(
        field_names=["Center", "Demand", "Selected", "Attendees"], align="r"
    )
    outreach_table.set_style(prettytable.MARKDOWN)
    centers = list(centers)
    centers.sort()
    for center in centers:
        outreach_table.add_row([
            center,
            demand_all.get(center, 0),
            selected_all.get(center, 0),
            attendees_all.get(center, 0)
        ])

    return f"### Outreach\n{outreach_table}\n"


def list_participant_hours_development(reports: List[Report]):
    """
    Create an overview over the development of the participant hours.

    This will include the monthly and yearly development.

    Args:
        reports:
            The list of reports for which the participant hours are to be
            included.

    Returns:
        A sequence of CSV lines in the format
        YYYY-MM, <monthly_sum>, <overall_sum>, <yearly_sum>
    """
    months = range(1, 1 + 12)  # Start counting months at index 1, for 12 months
    total_summary = {}

    for report in reports:
        date: datetime = report.dates[0]
        year = date.year
        month = date.month
        if year not in total_summary:
            total_summary[year] = {month: 0 for month in months}
        total_summary[year][month] += report.participant_hours

    lines = []
    overall_total = 0
    for year in total_summary:
        yearly_total = 0
        for month in total_summary[year]:
            monthly_total = total_summary[year][month]
            yearly_total += monthly_total
            overall_total += monthly_total
            lines.append(
                f"{year}-{month:02}, "
                f"{monthly_total}, "
                f"{overall_total}, "
                f"{yearly_total} "
            )
    return "\n".join(lines)
