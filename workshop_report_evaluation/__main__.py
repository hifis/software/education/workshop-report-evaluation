from datetime import datetime
from typing import List

import click
import pathlib

import evaluation

from report import Report
from report_reader import ReportReader


YAML_FILE_SUFFIXES = [".yml", ".yaml"]
""" The acceptable suffixes for report files. """


@click.command()
@click.option(
    "-r", "--report-directory",
    default=".",
    help="The path to the directory containing the reports",
    type=click.Path(
        exists=True,
        dir_okay=True,
        path_type=pathlib.Path
    )
)
@click.option(
    "-c", "--by-center",
    default=None,
    help="""
        The abbreviation of a specific center for which to generate reports.
        Only reports attributed to this center (i.e. center name is in the 
        'center' section of the report) will be included in the evaluation. 
        By default, no filter will be applied.
        """
)
@click.option(
    "--since",
    default=None,
    help="""
        The earliest date from when the reports should be considered.
        Any reports before this date will be discarded.
        By default, no filter will be applied.
        """,
    type=click.DateTime()
)
@click.option(
    "--until",
    default=None,
    help="""
        The latest date up to when the reports should be considered.
        Any reports after this date will be discarded.
        By default, no filter will be applied.
        """,
    type=click.DateTime()
)
@click.option(
    "--year",
    default=None,
    help="""
        Set the year to be evaluated. 
        Implies --since <year>-01-01 and --until <year>-12-31.
        Will override other values for --since and --until.
    """,
    type=int
)
@click.option(
    "--validate-only",
    default=False,
    is_flag=True,
    help="""
        Set this option to only run the validation part.
        If the validation is successful. the program will return with exit code 
        0, otherwise it will use a non-zero exit code.
    """
)
def run_evaluation(
        report_directory,
        by_center,
        since, until, year,
        validate_only
) -> None:
    """
    Run the evaluation on the given report folder.
    Args:
        report_directory:
            The path to the directory itself. It should point to an existing
            directory. By default, the current working directory will be used.
            The directory will be searched recursively for any YAMl files,
            which may contain reports.
        by_center:
            The name of a center for which the reports are to be generated.
            If not given, no filter for the center will be applied.
        since:
            The date from when reports should be considered. Reports from
            before that date will be ignored in the reporting.
            If not given, no filter for the minimum date will be applied.
        until:
            The date up to when reports should be considered. Reports from
            after that date will be ignored in the reporting.
            If not given, no filter for the maximum date will be applied.
        year:
            The year to be evaluated. Sets `since` to <year>-01-01
            and `until` to <year>-12-31.
            Defaults to None.
        validate_only:
            (Default=False) A boolean flag to indicate whether only the
            validation of reports should be done. If the validation is
            successful, the tool will exit with error code ``0``.
            Otherwise, a non-zero return code will be used. Note that the exact
            return code is determined by ``click``.
    """
    if year:
        since = datetime(year=year, month=1, day=1)
        until = datetime(year=year, month=12, day=31)

    report_reader = ReportReader()
    all_reports: List[Report] = report_reader.read_reports(report_directory)

    print(report_reader.validation_report(errors_only=True))

    if validate_only:
        if not report_reader.validation_successful:
            raise click.ClickException("Validations were not successful")
            # NOTE: raising a ClickException is clicks' way of indicating an
            # abnormal program exit and will trigger a non-zero return code.
        else:
            return

    # Build the list of all filters for reports
    filters = list()
    filters.append(lambda r: r.is_done) # Filter out canceled workshops
    if by_center:
        filters.append(lambda r: by_center in r.by_centers)
        print(f"Filter: No reports except from center {by_center} will be "
              f"considered")
    if since:
        filters.append(lambda r: r.dates[0] >= since.date())
        print(f"Filter: No reports before {since.date()} will be considered.")
    if until:
        filters.append(lambda r: r.dates[0] <= until.date())
        print(f"Filter: No reports after {until.date()} will be considered.")

    # Apply the filters
    filtered_reports = all_reports
    for filter_step in filters:
        filtered_reports = filter(filter_step, filtered_reports)

    complete_workshops: List[Report] = list(filtered_reports)
    # Run evaluation
    print(
        evaluation.generate_summary(complete_workshops),
        evaluation.evaluate_nps(complete_workshops),
        evaluation.list_sources(complete_workshops),
        evaluation.list_participation_details(complete_workshops),
        evaluation.create_outreach_table(complete_workshops),
        evaluation.list_participant_hours_development(complete_workshops),
        sep="\n"
    )


if __name__ == "__main__":
    run_evaluation()
