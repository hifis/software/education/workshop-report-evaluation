import re
import yaml

from collections.abc import Iterable
from pathlib import Path
from typing import Optional, Union, Callable, List, Any

from report import Report
from validation import (
    field_present, not_none, positive_value,
    participant_dictionary_valid, ValidationResult, centers_list_valid,
    center_name_valid, Validator
)


class YamlField:
    """
    Represents a field in the YAML file of the reports.

    The validators are executed after loading the report file.
    """

    def __init__(
            self,
            yaml_name: str,
            required: bool,
            expected_type: type,
            attribute_name: Optional[str] = None,
            validation_functions: Optional[
                Iterable[Callable[[Any, ...], ValidationResult]]
            ] = None
    ):
        """
        Describe a new field in the Report's YAML file.

        Args:
            yaml_name:
                The string used in the YAML file to identify the field.
            required:
                Whether the field must be present for the YAML file to be valid.
            expected_type:
                The data type expected for the value of the given field.
            attribute_name:
                (Optional) The name of the corresponding attribute in the
                Report class.
            validation_functions:
                (Optional) A set of additional validator callables to be
                 applied to the value of the field. Each validator should be a
                 callable that accepts the signature::

                    (Any, ...) -> bool

                where the ReportReader and Report may be used for callbacks or
                triggering of further validations if required. The string is to
                be used for the name of the attribute of the report that is to
                be validated.
                Lastly, the actual value to be evaluated is passed.
        """
        self.yaml_name = yaml_name
        self.attribute_name = attribute_name if attribute_name else yaml_name
        self.required = required
        self.expected_type = expected_type
        self.validation_functions = (
            validation_functions if validation_functions else set()
        )

    def validate(self, raw_yaml: dict, validator: Validator) -> (str, Any):
        """
        Run the validation for this YAML-field.

        Args:
            raw_yaml:
                The dictionary from the yaml file from which this field is to be
                extracted and validated.
            validator:
                The Validator-instance to be used to run the validations and
                record the results.

        Returns:
            A key-value pair for use in constructing Record kwargs.
            The key corresponds to the attribute_name, while the value is the
            one extracted and verified from the YAML-data.
        """

        # Validate: Field required
        if not validator.run_validation(
                field_name=self.yaml_name,
                validation_function=field_present,
                inputs=(self.yaml_name, raw_yaml),
                may_fail=not self.required
        ):
            raise KeyError("Can not instantiate report due to missing fields")

        raw_data = raw_yaml[self.yaml_name]

        # Validate: Field not none
        if not validator.run_validation(
                field_name=self.yaml_name,
                validation_function=not_none,
                inputs=(raw_data,)  # !!! Syntax: Tuple with one element
        ):
            raise ValueError("Field was not supposed to be None")

        # Validate: Field has correct type
        if not validator.run_validation(
                field_name=self.yaml_name,
                validation_function=isinstance,
                inputs=(raw_data, self.expected_type),
        ):
            raise TypeError("Field had wrong type")

        for to_run in self.validation_functions:
            validator.run_validation(
                field_name=self.attribute_name,
                validation_function=to_run,
                inputs=(raw_data,)
            )
        return self.attribute_name, raw_data


class ReportReader:
    """
    Reads Reports from files and validates them while doing so.
    """

    _YAML_FILE_SUFFIXES = [".yml", ".yaml"]
    """ The acceptable suffixes for report files. """

    _FILENAME_REGEX: str = (
        r"(?P<year>\d{4})-"
        r"(?P<month>\d{2})-"
        r"(?P<day>\d{2})-"
        r"(?P<organizer>\w+)"
        r"(-(?P<counter>\d+))?"
    )
    """ 
    A regular expression to take apart the expected pieces of the filename.
    It only operates on the file stem and is independent of the actual suffix. 
    """

    _YAML_FIELDS = {
        YamlField("dates", required=True, expected_type=Iterable),
        YamlField("centers", required=True, expected_type=Iterable,
                  attribute_name="by_centers",
                  validation_functions={centers_list_valid}),
        YamlField("duration", required=True, expected_type=Union[float, int],
                  validation_functions={positive_value}),
        YamlField("capacity", required=True, expected_type=int,
                  validation_functions={positive_value}),
        YamlField("demand", required=True, expected_type=dict,
                  validation_functions={participant_dictionary_valid}),
        YamlField("selected", required=True, expected_type=dict,
                  validation_functions={participant_dictionary_valid}),
        YamlField("attendees", required=True, expected_type=dict,
                  validation_functions={participant_dictionary_valid}),
        YamlField("links", required=False, expected_type=Iterable),
        YamlField("tags", required=False, expected_type=Iterable),
        YamlField("status", required=False, expected_type=str),
        YamlField("nps", required=False, expected_type=dict)
    }
    """
    The fields that can be expected in the YAML files of reports.
    """

    def __init__(self):
        self.validators: List[Validator] = list()

    def report_from_file(self, report_file: Path) -> Report:
        """
        Create a new report from a YAML file.

        Validation checks will be run while doing so. The validation results
        will be stored in the self-instance of ReportReader as an entry in the
        ``validation_results`` dictionary.

        Args:
             report_file:
                Path to the YAML file containing the report

        Returns:
            A newly generated report object that contains the data from the
            report file.

        Raises:
            KeyError:
                If one or more required fields were not present in the report
                YAML file.
            TypeError:
                If the instantiation of the report failed.
        """

        validator = Validator(report_file.name)
        self.validators.append(validator)

        matches = re.search(ReportReader._FILENAME_REGEX, report_file.stem)
        organizer: str = matches.group("organizer")
        validator.run_validation(
            field_name="organizer",
            validation_function=center_name_valid,
            inputs=(organizer,)
        )

        with open(report_file) as input_stream:
            raw_data = yaml.load(stream=input_stream, Loader=yaml.FullLoader)

        report_status = raw_data.get("status", Report.LABEL_STATUS_DONE)
        if report_status != Report.LABEL_STATUS_DONE:
            raise KeyError("Report was cancelled and might be incomplete.")

        valid_values_from_yaml: dict = {
            "file_name": report_file.name,
            "organizer": organizer,
            "status": report_status
        }
        """
        A dictionary with kwargs used to construct the Report.
        """

        for field in ReportReader._YAML_FIELDS:
            try:
                key, value = field.validate(
                    raw_yaml=raw_data,
                    validator=validator
                )
                valid_values_from_yaml[key] = value
            except KeyError:
                continue
            except ValueError:
                continue
            except TypeError:
                continue

        return Report(**valid_values_from_yaml)

    def read_reports(self, report_directory: Path) -> List[Report]:
        """
        Load reports from the files in a given directory.

        Reports that fail to validate will output a notification and then be
        skipped.

        Args:
            report_directory:
                The path to the directory containing the report files.
                Subdirectories are considered as well. Reports are given in the
                YAML format. Files that do not have an accepted suffix will be
                silently skipped.

        Returns:
            A list containing the successfully loaded reports.
        """
        file_list = [
            file for file in report_directory.rglob("*")
            if file.suffix in ReportReader._YAML_FILE_SUFFIXES
        ]
        loaded_reports = []
        for file in file_list:
            try:
                new_report = self.report_from_file(file)
                loaded_reports.append(new_report)
            except KeyError:
                print(f"Skipped incomplete report {file.name}")
                continue

        return loaded_reports

    def validation_report(self, errors_only=True) -> str:
        """
        Generate a Markdown-formatted validation report.

        Args:
            errors_only:
                (Optional, Default: True) Determines whether the report should
                be limited to failed validations only.

        Returns:
            A string containing the validation report in Markdown format.
        """

        output = "# Validation Report\n"

        for validator in self.validators:
            output += validator.validation_summary(errors_only=errors_only)

        return output

    @property
    def validation_successful(self) -> bool:
        """
        Check if no errors occurred during the report validation.

        If warnings (i.e. validations that failed, but were allowed to do so)
        were raised, they will not be counted towards the errors. A report can
        thus validate as successful, despite warnings being present.

        Returns:
            True, if no validation yielded an error;
            False, if any validation errors occurred.
        """
        for validator in self.validators:
            if validator.validation_errors:
                return False
        return True
