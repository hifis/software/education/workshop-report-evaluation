"""
Helper functions for validation purposes.
"""
import operator
from functools import reduce
from typing import Iterable, Callable, Tuple, List, Any, Dict

from center_names import VALID_CENTER_ABBREVIATIONS


ValidationResult = bool | Iterable["Validation"]
"""
A shorthand type for the possible results of a validation.
Either the validation was successful or failed, or spawns a set of 
sub-validations to be evaluated first.
"""


class Validation:
    """
    Validates a certain condition of a field/attribute of a given object.

    Examples:
        Assume an object ``my_object`` with an attribute ``value``.
        To validate if the ``value`` is an integer with the ``isinstance``-
        built-in function you may create a validation like this::

            Validation(
                field_name="value",
                validation_function=isinstance,
                inputs=(my_object.value, int)
            )

        The validator will call ``isinstance(my_object.value, int)`` during the
        initialization and store the resulting boolean value in the
        ``passed``-attribute.
    """

    def __init__(
            self,
            field_name: str,
            validation_function: Callable[[...], ValidationResult],
            inputs: Tuple,
            may_fail: bool = False
    ):
        """
        Create a new Validation and run the check described by it.

        The validation result is stored in the `passed`-attribute.
        Args:
            field_name:
                The name of the field or attribute of the object that is to be
                validated.

                **NOTE:** This will not resolve the field in any way and is only
                used for logging the validation. The actual value of the field
                has to be given as an argument to the ``validation_function``-
                callable as a part of the ``inputs``-tuple.
            validation_function:
                A callable (e.g. function, lambda-expression) that is used for
                validation. The callable will receive the ``inputs`` parameter
                as arguments and is expected to return a boolean value
                indicating whether the check has been passed (return True) or
                not (return False). Alternatively, a validation_function may
                return an iterable containing sub-validations to be run to
                determine whether the validation was successful.
            inputs:
                A tuple of the values used as positional arguments for the
                ``validation_function``-callable. The tuple will automatically
                be unpacked. If you only have one argument to pass in, take care
                to note it down as ``(my_value,)`` to prevent issues with the
                unpacking of the argument.
            may_fail:
                (Optional, Default: False) Indicates whether the validation may
                fail without creating an issue. Intended for marking optional
                validation checks that should only trigger a warning, instead
                of an error.
        """
        self.field_name = field_name
        self.inputs = inputs
        self.validation_function = validation_function
        self.may_fail = may_fail
        self.spawned_validations = list()
        self.passed = None

        self._run()

    @property
    def is_error(self) -> bool:
        return not self.passed and not self.may_fail

    @property
    def is_warning(self) -> bool:
        return not self.passed and self.may_fail

    def _run(self) -> None:
        """
        Run the validation function and potential sub-validation.

        The validation function may have a boolean result or yield a list of
        follow-up validations that have to be run in order to establish the
        success or failure of a validation.

        In the latter case, all of these sub-validations are run as well and
        only if they all passed (or were allowed to fail) this overarching
        validation counts as passed as well.
        """
        validator_result = self.validation_function(*self.inputs)
        if isinstance(validator_result, bool):
            self.passed = validator_result
            return
        elif isinstance(validator_result, Iterable):
            for sub_validation in validator_result:  # type: Validation
                self.spawned_validations.append(sub_validation)
                sub_validation._run()

            # Only passed when all spawned validations passed or were allowed
            # to fail
            self.passed = reduce(
                operator.and_,
                [
                    entry.passed or entry.may_fail
                    for entry in self.spawned_validations
                ]
            )
        else:
            raise TypeError(
                f"Can not deal with validation result of type "
                f"{type(validator_result)}"
            )

    def __str__(self):
        prefix = "OK  " if self.passed else "WARN" if self.may_fail else "FAIL"
        postfix = "(allowed to fail)" if self.may_fail else ""
        return (
            f"{prefix} FIELD = {self.field_name:20} "
            f"CHECK = {self.validation_function.__name__} {postfix}"
        )

    def compact(self, level=0) -> Dict["Validation", int]:
        """
        Flattens the tree of spawned validations recursively.

        Returns:
            A dictionary containing the validation itself and all spawned
            validations as keys and the nesting level as values.
        """
        flattened = {self: level}
        for spawned in self.spawned_validations:
            flattened |= spawned.compact(level + 1)

        return flattened


class Validator:
    """
    A helper class to run and store validations for an object.
    """

    def __init__(self, validated: Any):
        """
        Create a new Validator to run and store validations of an object.

        Args:
            validated:
                The thing that has been validated.
                When generating representations it will be cast to string.
        """
        self.validated = validated
        self.ran_validations: List[Validation] = list()

    def run_validation(self, **kwargs) -> bool:
        """
        Store a validation for future reference.

        A given validation will be generated and stored in the
        ``ran_validations``-list.

        Args:
            kwargs:
                Will be passed to the constructor for the new
                Validation-instance. See Validation.__init__(…) for details.

        Returns:
            Whether the validation has passed.
        """
        validation = Validation(**kwargs)
        self.ran_validations.append(validation)

        return validation.passed

    @property
    def validation_errors(self) -> List[Validation]:
        """
        Filter for and return all validations that resulted in an error.

        Validations that failed, but were allowed to do so are not included.

        Returns:
            A list of all validations that failed but were not allowed to
            do so.
        """

        return list(
            filter(lambda r: r.is_error, self.ran_validations)
        )

    def validation_summary(self, errors_only: bool = True) -> str:
        """
        Obtain a markdown-style text summary of all validations run so far.

        Args:
            errors_only:
                (Optional, Default: True) Whether the generated summary should
                be limited to only include the validations that failed and were
                not allowed to do so. This is most useful for debugging.

        Returns:
            A summary text for the validations. It is formatted in a markdown
            style and sorted by validated elements, which are distinguished by
            an intermediate heading.
        """
        relevant_validations = (
            self.validation_errors if errors_only else self.ran_validations
        )

        if not relevant_validations:
            return ""

        output = f"\n## {self.validated}\n"

        for validation in relevant_validations:
            for validation_entry, level in validation.compact().items():
                output += " " * (4 * level) + f"* {validation_entry}\n"
        return output


# === Validation functions ===
# Validation functions can in principle take arbitrary arguments.
# Restrictions can apply by use case.
# Validation functions can return either (but not both at the same time):
# 1. a boolean value indicating success / failure of the validation
# 2. an iterable containing further validations to be undertaken

def field_present(field_name, dictionary) -> bool:
    return field_name in dictionary


def not_none(to_check) -> bool:
    return to_check is not None


def center_name_valid(to_check: str) -> bool:
    return to_check in VALID_CENTER_ABBREVIATIONS


def positive_value(to_check: int | float) -> bool:
    return to_check >= 0


def centers_list_valid(to_check: Iterable[str]) -> ValidationResult:
    if not to_check:
        return False
    return {
        Validation(
            field_name=f"{center_name}",
            validation_function=center_name_valid,
            inputs=(center_name,)
        )
        for center_name in to_check
    }


def participant_dictionary_valid(to_check: dict[str, int]) -> ValidationResult:

    # Each line in the to_check dictionary spawns validations
    # For the center name and whether the value is positive
    return {  # !!! Syntax: Nested generator expression ahead
        new_validation for center_name, participants in to_check.items()
        for new_validation in (
            Validation(
                field_name=f"{center_name}",
                validation_function=center_name_valid,
                inputs=(center_name,)
            ),
            Validation(
                field_name=f"{center_name}",
                validation_function=positive_value,
                inputs=(participants,)
            )
        )
    }
