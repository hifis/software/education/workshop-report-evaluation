from typing import Dict, Optional


class NPS:
    """
    Representation of the Net Promoter Score for an event.

    Attributes:
        score:
            Determines how likely it is that the rated event will be
            recommended to other people. Its range is ``-100 <= score <= 100``
        n:
            How many individual ratings were gathered to determine the
            score. A higher value indicates a more reliable rating.
    """

    def __init__(self, raw_data: Optional[Dict[str, int]] = None):
        """
        Create a new NPS instance from a dictionary with raw data.

        Args:
            raw_data:
                A dictionary containing the keys "score" and "n" which will
                be mapped to the respective attributes.
        """
        if raw_data is None:
            raw_data = {}

        self.score = raw_data.get("score", None)
        self.n = raw_data.get("n", None)

        if self.n is not None and self.n < 0:
            raise ValueError(
                "The amount of samples for an NPS can not be negative"
            )
        if self.score is not None and (self.score < -100 or self.score > 100):
            raise ValueError(
                "The NPS score must be in the range (-100; 100)"
            )

    def __bool__(self):
        """
        Obtain the boolean representation of an NPS.

        Returns:
            True, if the NPS is valid (i.e. the ``score`` and ``n`` are given
            and ``n`` > 0). False otherwise.

        """
        # This object is only useful if a score is given and the n is
        # actually positive
        return (
            (self.score is not None) and
            (self.n is not None) and
            (self.n > 0)
        )

    def __str__(self) -> str:
        return f"{self.score:=+4} (n = {self.n})"
