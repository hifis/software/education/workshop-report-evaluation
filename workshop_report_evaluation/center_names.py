"""
Convenience functionality for handling center names and abbreviations.
"""
from typing import Dict, List

# Custom Type names for clarity
CenterName = str
CenterNames = List[str]  # Use List for ordering
ValuePerCenter = Dict[CenterName, int]

# noinspection SpellCheckingInspection
VALID_CENTER_ABBREVIATIONS = {
    "awi",       # Alfred-Wegener-Institut
    "cispa",     # Helmholtz-Zentrum für Informationssicherheit
    "desy",      # Deutsches Elektronen-Synchotron
    "dkfz",      # Deutsches Krebsforschungszentrum
    "dzne",      # Deutsches Zentrum für Neurodegenerative Erkrankungen
    "dlr",       # Deutsches Luft- und Raumfahrtzentrum
    "fzj",       # Forschungszentrum Jülich
    "geomar",    # Helmholtz-Zentrum für Ozeanforschung Kiel
    "gfz",       # Geoforschungszentrum Potsdam
    "gsi",       # Helmholtz-Zentrum für Schwerionenforschung
    "hereon",    # Helmholtz-Zentrum Geesthacht
    "hzb",       # Helmholtz-Zentrum Berlin
    "hzdr",      # Helmholtz-Zentrum Dresden-Rossendorf
    "hzi",       # Helmholtz-Zentrum für Infektionsforschung
    "hzm",       # Helmholtz-Zentrum München
    "kit",       # Karlsruhe Institute of Technology
    "mdc",       # Max-Delbrück-Center
    "ufz",       # Umweltforschungszentrum Leipzig
    "other",     # Helmholtz, but not part of a center
    "external",  # Not in Helmholtz
    "unknown"    # Origin of participant could not be determined
}
