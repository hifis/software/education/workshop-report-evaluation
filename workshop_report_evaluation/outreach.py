

class Outreach:
    """
    Encapsulates the outreach from one center towards another one.

    The `from_center` is the source of the outreach, the `to_center` is the
    center which the outreach is targeted to.
    The outreach itself is given by the metrics of measured `demand` by the
    target center, the `selected` participants from this demand, and the
    actual `attendees`.
    """

    def __init__(
            self,
            from_center: str,
            to_center: str,
            demand: int,
            selected: int,
            attendees: int
    ):
        """
        Create a new outreach measurement.

        Args:
            from_center:
                The center which initiated the outreach
            to_center:
                The center targeted in the outreach
            demand:
                The amount of people who asked to participate in any events
            selected:
                The amount of people from the `to_center` who were given a
                seat in any events
            attendees:
                The actual attendees of `to_center` in events of `from_center`
        """
        self.from_center = from_center
        self.to_center = to_center
        self.demand = demand
        self.selected = selected
        self.attendees = attendees

    def __add__(self, other):
        if not isinstance(other, Outreach):
            raise ValueError(f"Can not add {type(other)} to Outreach instance")

        if other.from_center != self.from_center:
            raise ValueError("Outreach objects have different from_center")

        if other.to_center != self.to_center:
            raise ValueError("Outreach objects have different to_center")

        return Outreach(
            self.from_center,
            self.to_center,
            self.demand + other.demand,
            self.selected + other.selected,
            self.attendees + other.attendees
        )

    def __copy__(self) -> "Outreach":
        return Outreach(
            from_center=self.from_center,
            to_center=self.to_center,
            demand=self.demand,
            selected=self.selected,
            attendees=self.attendees
        )
