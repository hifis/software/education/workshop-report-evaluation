from datetime import datetime
from typing import List, Optional, Set, Union, Iterable

from center_names import CenterName, ValuePerCenter
from nps import NPS
from outreach import Outreach


class Report:
    """
    A Report covers a singular event.
    """

    LABEL_STATUS_DONE: str = "done"
    """ 
    Indicates that the workshop was held properly.
    Will be assumed to be the case if no other status is explicitly set.
    """

    def __init__(
            self,
            file_name: str,
            organizer: CenterName,
            dates: Iterable[datetime.date],
            by_centers: Iterable[CenterName],
            duration: Union[int, float],
            capacity: int,
            demand: ValuePerCenter,
            selected: ValuePerCenter,
            attendees: ValuePerCenter,
            links: Optional[Iterable[str]] = None,
            tags: Optional[Iterable[str]] = None,
            remarks: Optional[Iterable[str]] = None,
            status: Optional[str] = None,
            nps: Optional[dict[str, int]] = None
    ):
        self.file_name = file_name
        self.organizer = organizer
        self.by_centers = list(by_centers)
        self.dates = list(dates)
        self.duration = duration
        self.capacity = capacity
        self.demand = demand
        self.selected = selected
        self.attendees = attendees
        self.links = set(links) if links else set()
        self.tags = list(tags) if tags else list()
        self.remarks = set(remarks) if remarks else set()
        self.status = status if status else Report.LABEL_STATUS_DONE
        self.nps = NPS(nps) if nps else NPS()

        self.dates.sort()
        self.tags.sort()

    @property
    def total_demand(self) -> int:
        """
        Get the overall demand of the workshop across all centers.

        Returns:
            The amount of people who wanted to participate in the workshop.
        """
        return sum(self.demand.values())

    @property
    def total_selected(self) -> int:
        """
        Get the overall number of admitted participants across all centers.

        Returns:
            The amount of people who were offered a seat in the workshop.
        """
        return sum(self.selected.values())

    @property
    def total_attendees(self) -> int:
        """
        Get the total amount of participants in the workshop.

        Returns:
            The amount of people who attended the workshop.
        """
        return sum(self.attendees.values())

    @property
    def participant_hours(self) -> int:
        """
        Get the total teaching hours accumulated over all the participants.

        Returns:
            The participant-hours.
        """
        return self.total_attendees * self.duration

    @property
    def coverage(self) -> Optional[float]:
        """
        Get the coverage of this workshop.
        Coverage is calculated as the relative amount of the interested
        people who were actually offered a seat in the workshop.

        Returns:
            The coverage as a relative value. (i.e. a coverage of 1 = 100%)
            means that everyone who was interested also got a seat.
            If the demand is not given, the result will be None (due
            to the resulting division by 0)
        """
        try:
            return self.total_selected / self.total_demand
        except ZeroDivisionError:
            return None

    @property
    def no_show_rate(self) -> Optional[float]:
        """
        Get the no-show-rate of the workshop.
        The no-show-rate is the relative amount of selected attendees who
        did not show up in the workshop.

        Returns:
            The no-show-rate as relative value. A no-show-rate of 1 = 100%
            means no one came to the workshop. The no-show-rate may be
            negative if people showed up to the workshop even though they
            were not selected. If no number was given for the selected
            persons, the result will be None (due to the resulting division
            by 0)
        """
        try:
            absentees = self.total_selected - self.total_attendees
            return absentees / self.total_selected
        except ZeroDivisionError:
            return None

    @property
    def booking(self):  # Selected / Capacity
        """
        Get the booking rate of the workshop.
        The booking rate is defined as the capacity that was covered by the
        amount of selected participants.

        Returns:
            The booking rate as a relative value. A booking rate greater
            than 1 = 100% means that the course was overbooked.
            If no capacity was given, the result will be None (due to the
            resulting division by 0)
        """
        try:
            return self.total_selected / self.capacity
        except ZeroDivisionError:
            return None

    @property
    def utilization(self) -> Optional[float]:
        """
        Get the utilization rate of the workshop.
        The utilization determines how well the available capacity was
        actually used in the end.

        Returns:
            The utilization rate as a relative value. A utilization of 1 =
            100% means the workshops capacities were used to the full
            extent. A utilization over 1 implies that the workshop served
            more participants than it was planned for.
            If no capacity is given, the result will be None (due to the
            resulting division by 0)
        """
        try:
            return self.total_attendees / self.capacity
        except ZeroDivisionError:
            return None

    @property
    def outreach(self) -> List[Outreach]:
        """
        Get a list of outreaches from this workshop.
        Each outreach comes from the organizing center and is aimed towards
        each of the reached centers that expressed interest or sent
        participants.

        Returns:
            A list of Outreach objects indicated by this report
        """

        outreaches: List[Outreach] = []

        reached_centers: Set[str] = set().union(
            set(self.demand.keys()),
            set(self.selected.keys()),
            set(self.attendees.keys())
        )

        for center in reached_centers:
            new_outreach = Outreach(
                from_center=self.organizer,
                to_center=center,
                demand=self.demand.get(center, 0),
                selected=self.selected.get(center, 0),
                attendees=self.attendees.get(center, 0)
            )

            outreaches.append(new_outreach)
        return outreaches

    @property
    def is_done(self) -> bool:
        """
        Check if a workshop is marked as done.

        Returns:
            True, if the report is marked as "Done", False otherwise
        """
        return self.status == Report.LABEL_STATUS_DONE
